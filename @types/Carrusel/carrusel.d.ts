interface ICarrusel {
  id: string
  nombre: string
  imagen: string
  url: string
  descripcion: string
}