import React from 'react'
import {
  SafeAreaView,
  Text
} from 'react-native'
import { Header as HeaderRNE } from '@rneui/themed'
import styles from "../../../styles"

const Header = ({ navigation }) => {
  return (
    <SafeAreaView>
      <HeaderRNE
        leftComponent={
          <Text
            style={styles.heading}
            onPress={() => { navigation.navigate("Home") }}
          >
            Home
          </Text>
        }
        rightComponent={
          <Text
            style={styles.heading}
            onPress={() => { navigation.navigate("Becas") }}
          >
            Becas
          </Text>

        }
      />
    </SafeAreaView>
  )
}

export default Header