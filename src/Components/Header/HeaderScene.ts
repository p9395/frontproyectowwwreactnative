import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Header from '.'

const mapStateToProps = (state: IRootState) => {
  return {
  }
}

const mapDispatchToProps = (dispatch: ThunkDispatchType) => bindActionCreators({
}, dispatch)

interface IOwnProps {
  navigation: IObj
}

type TMapStateToProps = ReturnType<typeof mapStateToProps>
type TMapDispatchToProps = ReturnType<typeof mapDispatchToProps>

export type HeaderProps = IOwnProps & TMapStateToProps & TMapDispatchToProps
export default connect(mapStateToProps, mapDispatchToProps)(Header)
