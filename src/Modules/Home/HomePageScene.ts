import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import HomePage from '.'
import { obtenerBecas } from '../../Reducers/Becas/BecasActions'
import { obtenerNoticias } from '../../Reducers/Noticias/NoticiasActions'

const mapStateToProps = (state: IRootState) => {
  return {
  }
}

const mapDispatchToProps = (dispatch: ThunkDispatchType) => bindActionCreators({
  obtenerBecas,
  obtenerNoticias,
}, dispatch)

interface IOwnProps {
  navigation: IObj
}

type TMapStateToProps = ReturnType<typeof mapStateToProps>
type TMapDispatchToProps = ReturnType<typeof mapDispatchToProps>

export type HomePageProps = IOwnProps & TMapStateToProps & TMapDispatchToProps
export default connect(mapStateToProps, mapDispatchToProps)(HomePage)
