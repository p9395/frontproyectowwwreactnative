import * as React from 'react'
import { ScrollView, View } from 'react-native'
import { HomePageProps } from './HomePageScene'
import CarruselBecas from './BecasCarrusel'
import CarruselNoticias from './NoticiasCarrusel'
import Headers from "../../Components/Header"

class HomePage extends React.Component<HomePageProps> {
  constructor(props: any) {
    super(props)
  }

  componentDidMount() {
    this.props.obtenerBecas()
    this.props.obtenerNoticias()
  }

  render() {
    return (
      <ScrollView>
      <Headers navigation={this.props.navigation} />
        <View>
          <CarruselBecas />
          <CarruselNoticias />
        </View>
      </ScrollView>
    )
  }
}

export default HomePage
