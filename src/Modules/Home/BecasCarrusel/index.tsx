import { connect } from 'react-redux'
import { StatusBar } from "expo-status-bar"
import React from "react"
import {
  Text,
  View,
  Dimensions,
  SafeAreaView,
  Animated,
} from "react-native"
import { Card } from '@rneui/themed'
import styles from "../../../../styles"

const width = Dimensions.get("window").width
const ANCHO_CONTENEDOR = width * 0.7
const ESPACIO_CONTENEDOR = (width - ANCHO_CONTENEDOR) / 2
const ESPACIO = 10

function CarruselBecas({ becas }) {
  const becasData: IBeca[] = becas.becas
  const scrollX = React.useRef(new Animated.Value(0)).current
  if (becasData && becasData.length != 0) {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar hidden />
        <Animated.FlatList
          onScroll={Animated.event(
            [{ nativeEvent: { contentOffset: { x: scrollX } } }],
            { useNativeDriver: true }
          )}
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          snapToAlignment="start"
          contentContainerStyle={{
            paddingTop: 100,
            paddingHorizontal: ESPACIO_CONTENEDOR,
          }}
          snapToInterval={ANCHO_CONTENEDOR}
          decelerationRate={0}
          scrollEventThrottle={16}
          data={becasData}
          keyExtractor={(item) => item.nombre}
          renderItem={({ item, index }) => {
            if (item.principalPage) {
            const inputRange = [
              (index - 1) * ANCHO_CONTENEDOR,
              index * ANCHO_CONTENEDOR,
              (index + 1) * ANCHO_CONTENEDOR,
            ]
            const scrollY = scrollX.interpolate({
              inputRange,
              outputRange: [0, -50, 0],
            })
            return (
              <View style={{ width: ANCHO_CONTENEDOR }}>
                <Animated.View
                  style={{
                    marginHorizontal: ESPACIO,
                    padding: ESPACIO,
                    borderRadius: 34,
                    backgroundColor: "#fff",
                    alignItems: "center",
                    transform: [{ translateY: scrollY }],
                  }}
                >
                  <Card containerStyle={styles.posterImage}>
                    <Card.Title>{item.nombre}</Card.Title>
                    <Card.Divider />
                    <Card.Image
                      style={{ padding: 0 }}
                      source={{
                        uri: item.categoria == "D" ? `https://i.ibb.co/wSVWb9j/doctorado.png` :
                          item.categoria == "M" ? `https://i.ibb.co/jVZwXtF/maestria.png` :
                            "",
                      }}
                    />
                    <Text style={{ margin: 10 }}>Pais: {item.pais.nombre}</Text>
                    <Text style={{ margin: 10 }}>Universidad: {item.universidad.nombre}</Text>
                  </Card>
                </Animated.View>
              </View>
            )
          }
        }}
        />
      </SafeAreaView>
    )
  } else {
    return null
  }
}

const mapStateToProps = (state: IRootState) => {
  return {
    becas: state.becas,
  }
}

export default connect(
  mapStateToProps,
)(CarruselBecas)