import * as React from 'react'
import { Text, View } from 'react-native'
import { ListItem, Avatar } from '@rneui/themed'
import { BecasProps } from './BecasScene'
import styles from '../../../styles'
import Header from "../../Components/Header/HeaderScene"

const list = [
  {
    name: 'Amy Farha',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
    subtitle: 'Vice President'
  },
  {
    name: 'Chris Jackson',
    avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
    subtitle: 'Vice Chairman'
  },
]

class Becas extends React.Component<BecasProps> {
  constructor(props: any) {
    super(props)
  }

  render() {
    const { becas } = this.props.becas

    return (
      <View>
        <Header navigation={this.props.navigation} />

        <View style={{ marginTop: 10 }}>
          {
            becas.map((beca, index) => (
              <ListItem key={index} bottomDivider>
                <Avatar source={{
                  uri: beca.categoria == "D" ? `https://i.ibb.co/wSVWb9j/doctorado.png` :
                  beca.categoria == "M" ? `https://i.ibb.co/jVZwXtF/maestria.png` :
                      "",
                }} />
                <ListItem.Content>
                  <ListItem.Title>{beca.nombre}</ListItem.Title>
                  <ListItem.Subtitle>Pais: {beca.pais.nombre} , Universidad: {beca.universidad.nombre}</ListItem.Subtitle>
                </ListItem.Content>
              </ListItem>
            ))
          }
        </View>
      </View>
    )
  }
}

export default Becas
