import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Becas from '.'

const mapStateToProps = (state: IRootState) => {
  return {
    becas: state.becas,
  }
}

const mapDispatchToProps = (dispatch: ThunkDispatchType) => bindActionCreators({
}, dispatch)

interface IOwnProps {
  navigation: IObj
}

type TMapStateToProps = ReturnType<typeof mapStateToProps>
type TMapDispatchToProps = ReturnType<typeof mapDispatchToProps>

export type BecasProps = IOwnProps & TMapStateToProps & TMapDispatchToProps
export default connect(mapStateToProps, mapDispatchToProps)(Becas)
