import awaitToJs from 'await-to-js'
import config from '../../../config'
import { makeRequest } from "../../utils/client"

export enum UniversidadActionTypes {
  SET_UNIVERSIDAD = 'universidad/SET_UNIVERSIDAD',
}

export const obtenerUniversidades = (): IThunkResult => {
  return async (dispatch) => {
    const [error, response] = await awaitToJs(makeRequest({
      query: `${config.urlBackend}/universidades`,
      httpVerb: "get"
    }))

    if (error) {
      throw error
    }

    let universidades = response.data ? response.data.response : []
    dispatch(set_universidades(universidades))
  }
}

export const set_universidades = (value: IBeca[]): IAction => ({
  type: UniversidadActionTypes.SET_UNIVERSIDAD,
  value,
})