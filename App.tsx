import * as React from 'react'
import { Provider } from 'react-redux'
import { View, SafeAreaView } from 'react-native'
import HomePage from "./src/Modules/Home/HomePageScene"
import Becas from "./src/Modules/Becas/BecasScene"
//import MainStack from "./src/navigation/MainStack"
import { configureStore } from './src/Redux/setupStore'
import styles from "./styles"

import { createNativeStackNavigator } from "@react-navigation/native-stack"
import { NavigationContainer } from "@react-navigation/native"

const store = configureStore()
const Stack = createNativeStackNavigator()

export default function App() {
  return (
    <Provider store={store as any}>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}
        >
          <Stack.Screen
            name="Home"
            component={HomePage}
          />
          <Stack.Screen
            name="Becas"
            component={Becas}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  )
}
